﻿namespace BinEncoder
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCOM = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonBIN = new System.Windows.Forms.Button();
            this.textBoxBIN = new System.Windows.Forms.TextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.listBoxStatus = new System.Windows.Forms.ListBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.checkBoxHEX = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbCOM
            // 
            this.cbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cbCOM.FormattingEnabled = true;
            this.cbCOM.Location = new System.Drawing.Point(94, 12);
            this.cbCOM.Name = "cbCOM";
            this.cbCOM.Size = new System.Drawing.Size(406, 28);
            this.cbCOM.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "COM Port:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonBIN
            // 
            this.buttonBIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBIN.Location = new System.Drawing.Point(3, 44);
            this.buttonBIN.Name = "buttonBIN";
            this.buttonBIN.Size = new System.Drawing.Size(86, 28);
            this.buttonBIN.TabIndex = 9;
            this.buttonBIN.Text = "BIN";
            this.buttonBIN.UseVisualStyleBackColor = true;
            this.buttonBIN.Click += new System.EventHandler(this.buttonBIN_Click);
            // 
            // textBoxBIN
            // 
            this.textBoxBIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBIN.Location = new System.Drawing.Point(94, 45);
            this.textBoxBIN.Name = "textBoxBIN";
            this.textBoxBIN.ReadOnly = true;
            this.textBoxBIN.Size = new System.Drawing.Size(406, 26);
            this.textBoxBIN.TabIndex = 10;
            // 
            // buttonLoad
            // 
            this.buttonLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad.Location = new System.Drawing.Point(210, 86);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(125, 28);
            this.buttonLoad.TabIndex = 11;
            this.buttonLoad.Text = "LOAD";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // listBoxStatus
            // 
            this.listBoxStatus.FormattingEnabled = true;
            this.listBoxStatus.Location = new System.Drawing.Point(48, 131);
            this.listBoxStatus.Name = "listBoxStatus";
            this.listBoxStatus.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxStatus.Size = new System.Drawing.Size(452, 290);
            this.listBoxStatus.TabIndex = 12;
            // 
            // buttonExit
            // 
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.Location = new System.Drawing.Point(231, 430);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(82, 28);
            this.buttonExit.TabIndex = 13;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // checkBoxHEX
            // 
            this.checkBoxHEX.AutoSize = true;
            this.checkBoxHEX.Location = new System.Drawing.Point(48, 427);
            this.checkBoxHEX.Name = "checkBoxHEX";
            this.checkBoxHEX.Size = new System.Drawing.Size(103, 17);
            this.checkBoxHEX.TabIndex = 14;
            this.checkBoxHEX.Text = "Show BINDATA";
            this.checkBoxHEX.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 470);
            this.Controls.Add(this.checkBoxHEX);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.listBoxStatus);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.textBoxBIN);
            this.Controls.Add(this.buttonBIN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCOM);
            this.Name = "Main";
            this.Text = "BIN Hex Encoder";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCOM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonBIN;
        private System.Windows.Forms.TextBox textBoxBIN;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.ListBox listBoxStatus;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.CheckBox checkBoxHEX;
    }
}

