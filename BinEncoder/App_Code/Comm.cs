﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinEncoder.App_Code
{
    public class Comm
    {
        private SerialRead _mSerialRead;
        private Thread _oSerialRead;

        protected SerialPort SerialPort = new SerialPort();

        public SerialPort GetSP()
        {
            return SerialPort;
        }

        public bool CommEstablished()
        {
            try
            {
                return _mSerialRead._bCommEstablished;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool InitializeComm(string sPortName)
        {
            if (String.IsNullOrEmpty(sPortName)) return false;
            _mSerialRead = new SerialRead();
            SerialPort.DataBits = 8;
            SerialPort.Parity = Parity.None;
            SerialPort.StopBits = StopBits.One;
            SerialPort.Handshake = Handshake.None;
            SerialPort.PortName = sPortName;
            SerialPort.ReadTimeout = 1500;
            SerialPort.WriteTimeout = 1500;
            SerialPort.BaudRate = 115200;
            SerialPort.DtrEnable = true;
            SerialPort.RtsEnable = true;

            SerialPort.ReceivedBytesThreshold = 12; // Minimum is 6 in rare cases
            try
            {
                SerialPort.Open();
                SendCommand("");
                Thread.Sleep(50);
                while (SerialPort.BytesToRead > 0) SerialPort.ReadByte();
            }
            catch (Exception ex)
            {
                //ut.LogToFile(ex.Message);
                return false;
            }
            _oSerialRead = new Thread(_mSerialRead.ThreadController);
            _oSerialRead.SetApartmentState(ApartmentState.STA);
            _mSerialRead._ComPort = SerialPort;
            _oSerialRead.Start();

            // string sWho = WHOIS();
            // _mSerialRead._bCommEstablished = sWho.ToLower().Contains("rev");
            _mSerialRead._bCommEstablished = true;
            if (!_mSerialRead._bCommEstablished) CloseComm();
            return _mSerialRead._bCommEstablished;
        }

        public void CloseComm()
        {
            _mSerialRead._bCommEstablished = false;
            if (SerialPort.IsOpen)
            {
                SerialPort.Close();
            }
            try
            {
                _mSerialRead._Cancel = true;
            }
            catch
            {
            }
        }

        public string WHOIS()
        {
            return SendCommand("WHOIS");
        }

        #region Treadmill Controls

        public string STATUS()
        {
            return SendCommand("STATUS");
        }


        public string GETMAC()
        {
            return SendCommand("GETMAC");
        }

        public void SETMAC(string sMac)
        {
            string[] saMAC = sMac.Split(':');
            int[] iMAC = new int[6];
            for (var i = 0; i < saMAC.Length; i++)
            {
                iMAC[i] = Convert.ToInt32(saMAC[i], 16);
            }
            SendCommand("SETMAC " + iMAC[0] + " " + iMAC[1] + " " + iMAC[2] + " " + iMAC[3] + " " + iMAC[4] + " " + iMAC[5]);
        }


        public string GETUNITSERIAL()
        {
            return SendCommand("GETUNITSERIAL");
        }

        public string SETUNITSERIAL(string sSerialNumber)
        {
            return SendCommand("SETUNITSERIAL " + sSerialNumber);
        }


        public string GETPRESSURESERIAL()
        {
            return SendCommand("GETPRESSURESERIAL");
        }

        public string SETPRESSURESERIAL(string sSerialNumber)
        {
            return SendCommand("SETPRESSURESERIAL " + sSerialNumber);
        }

        public void FRURESET()
        {
            SendCommand("FRURESET 1");
        }


        #endregion

#region BIN Upload
        public string BinLoad(int NumBytes, uint uCheckSum)
        {
            return SendCommand("BINLOAD " + NumBytes + " " + uCheckSum);
        }

        public string BinData(string sHexData, bool bWait)
        {
            return SendCommand("BINDATA " + sHexData, bWait);
        }
        #endregion

        private string SendCommand(string sRequest, bool bWait = true)
        {
            _mSerialRead._manualEvent.Reset();
            try
            {
                SerialPort.Write(sRequest + "\r");
            }
            catch (Exception)
            {
                return "";
            }
            if (!bWait) return "";
            _mSerialRead._bCommEstablished = _mSerialRead._manualEvent.WaitOne(new TimeSpan(0, 0, 0, 0, 500));
            string sResponse = _mSerialRead.InputResponse;
            _mSerialRead.InputResponse = "";
            return sResponse;
        }

        private class SerialRead
        {

            public ManualResetEvent _manualEvent;
            public bool _Cancel { get; set; } = false;
            public SerialPort _ComPort { get; set; }
            public string InputResponse { get; set; } = "";
            public bool _bCommEstablished { get; set; } = false;
            private string sInputBuf;

            public SerialRead()
            {
                _manualEvent = new ManualResetEvent(false);
            }

            public void ThreadController()
            {
                _bCommEstablished = false;
                while (!_Cancel)
                {
                    try
                    {
                        Thread.Sleep(1);
                        if (ReadSerial())
                        {
                            InputResponse = sInputBuf;
                            sInputBuf = "";
                            _manualEvent.Set(); 
                            //Thread.Sleep(100);
                            //InputResponse = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        //ut.LogToFile(ex.Message);
                    }
                }
            }

            private bool ReadSerial()
            {
                try
                {
                    var iBytesToRead = _ComPort.BytesToRead;
                    if (iBytesToRead > 0)
                    {
                        for (var i = 0; i < iBytesToRead; i++)
                        {
                            try
                            {
                                var iByte = _ComPort.ReadByte();
                                if (iByte == 0xD)
                                {
                                    // "\r" Terminator
                                    //Debug.WriteLine(sInputBuf);
                                    return true;
                                }
                                // Build the string
                                sInputBuf += Convert.ToChar(iByte);
                            }
                            catch (TimeoutException te)
                            {
                                Debug.WriteLine(te.Message);
                            }
                        }
                    }
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public class StatusClass
        {
            public StatusClass(string sPacket)
            {
                RawStatus = sPacket;
                string[] saPacket = sPacket.Split('|');
                SequenceNum = UInt32.Parse(saPacket[0]);
                TimeTick = UInt32.Parse(saPacket[1]);
                Speedx10 = Int32.Parse(saPacket[2]);
                Incline = Int32.Parse(saPacket[3]);
                BodyWeight = Int32.Parse(saPacket[4]);
                Pressure = Int32.Parse(saPacket[5]);
                LoadCell1 = Int32.Parse(saPacket[6]);
                LoadCell2 = Int32.Parse(saPacket[7]);
                LoadCell3 = Int32.Parse(saPacket[8]);
                LoadCell4 = Int32.Parse(saPacket[9]);
                DecodeStatusReg(saPacket[10]);
            }

            private void DecodeStatusReg(string sReg)
            {

                //Decode The ErrorCode
                int iStatsReg = 0;
                try
                {
                    iStatsReg = Convert.ToInt32(sReg, 16);
                }
                catch (Exception)
                {
                }

                Running = ((iStatsReg & 0x01) == 0x01);
                Stopped = ((iStatsReg & 0x02) == 0x02);
                Paused = ((iStatsReg & 0x04) == 0x04);
                Diag = ((iStatsReg & 0x40) == 0x40);
                Calibrating = ((iStatsReg & 0x80) == 0x80);
                eStop = ((iStatsReg & 0x10) == 0x10);
                Latch = ((iStatsReg & 0x08) == 0x08);
                Blower = ((iStatsReg & 0x20) == 0x20);
                Ready = ((iStatsReg & 0x2000) == 0x2000);
                StatusCode = (eStatusCode)((iStatsReg >> 8) & 0x1F);
            }

            public enum eStatusCode
            {
                no_error = 0,
                deck_weight_error,
                user_under_weight_error,
                prefill_timeout_error,
                calpoints_error,
                pressure_sensor_error,
                overpressure_error,
                underpressure_error,
                residualpressure_error,
                loadcell_error,
                unused_10,
                unused_11,
                unused_12,
                unused_13,
                unused_14,
                unused_15,
                cal_1,
                cal_2,
                cal_3,
                cal_4
            }

            public string GetStatusText(eStatusCode eCode)
            {
                switch (eCode)
                {
                    case eStatusCode.no_error:
                        return "NONE";
                    case eStatusCode.deck_weight_error:
                        return "C01 - deck weight";
                    case eStatusCode.user_under_weight_error:
                        return "C02 - under weight";
                    case eStatusCode.prefill_timeout_error:
                        return "C03 - prefill timeout";
                    case eStatusCode.calpoints_error:
                        return "C04 - cal points";
                    case eStatusCode.pressure_sensor_error:
                        return "C05 - over pressure during Cal";
                    case eStatusCode.overpressure_error:
                        return "OOP - over pressure";
                    case eStatusCode.underpressure_error:
                        return "NPC - can not reach pressure";
                    case eStatusCode.residualpressure_error:
                        return "PRS - pressure sensor error";
                    case eStatusCode.loadcell_error:
                        return "C06 - load cell sensor";
                    case eStatusCode.unused_10:
                    case eStatusCode.unused_11:
                    case eStatusCode.unused_12:
                    case eStatusCode.unused_13:
                    case eStatusCode.unused_14:
                    case eStatusCode.unused_15:
                        return "Unknown";
                    case eStatusCode.cal_1:
                        return "Filling bag";
                    case eStatusCode.cal_2:
                        return "Measuring effective BW";
                    case eStatusCode.cal_3:
                        return "Building calibration curve";
                    case eStatusCode.cal_4:
                        return "Returning to 100% BW";
                    default:
                        return "UNKNOWN";
                }
            }

            public string RawStatus { get; private set; }
            public uint SequenceNum { get; private set; }
            public uint TimeTick { get; private set; } //times by .01 to get seconds
            public int Speedx10 { get; private set; } //times by .1 to get Speed
            public int Incline { get; private set; }
            public int BodyWeight { get; private set; }
            public int Pressure { get; private set; }
            public int LoadCell1 { get; private set; }
            public int LoadCell2 { get; private set; }
            public int LoadCell3 { get; private set; }
            public int LoadCell4 { get; private set; }

            public eStatusCode StatusCode { get; private set; }

            public bool Running { get; private set; } = false;
            public bool Stopped { get; private set; } = false;
            public bool Paused { get; private set; } = false;
            public bool Diag { get; private set; } = false;
            public bool Calibrating { get; private set; } = false;
            public bool eStop { get; private set; } = false;
            public bool Latch { get; private set; } = false;
            public bool Blower { get; private set; } = false;
            public bool Ready { get; private set; } = false;

            public void SetStatusFlags(bool bRunning, bool bStopped, bool bPaused, bool bDiag, bool bCal, bool beStop,
                bool bLatch, bool bBlower,
                bool bReady)
            {
                Running = bRunning;
                Stopped = bStopped;
                Paused = bPaused;
                Diag = bDiag;
                Calibrating = bCal;
                eStop = beStop;
                Latch = bLatch;
                Blower = bBlower;
                Ready = bReady;
            }

        }
    }

}
