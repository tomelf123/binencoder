﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using BinEncoder.App_Code;
using System.IO;
using System.Threading;

namespace BinEncoder
{
    public partial class Main : Form
    {
        private const int _nBytesPerRow = 16;
        private const int _nHeaderSize = 60;            //size of BIN header in bytes
        private const uint _uHeaderCheckSum = 0x1DE9;   //expected header checksum

        public Main()
        {
            InitializeComponent();
        }

        private List<COMPortInfo> oInfo;
        private Thread _oXferThread;
        ComPort oPortTemp;

        private void PopulateCOM()
        {
            oInfo = COMPortInfo.GetCOMPortsInfo();
            //string[] ports = SerialPort.GetPortNames();
            cbCOM.Items.Clear();
            ComPort oPort = new ComPort
            {
                PortName = "",
                DiplayText = "SELECT"
            };
            cbCOM.Items.Add(oPort);
            cbCOM.SelectedIndex = 0;

            foreach (COMPortInfo oTempPort in oInfo)
            {
                ComPort oPortTemp = new ComPort
                {
                    PortName = oTempPort.Name,
                    DiplayText = oTempPort.Description
                };
               cbCOM.Items.Add(oPortTemp); 
               if (oTempPort.Description.ToLower().Contains("at91"))
                {
                    cbCOM.SelectedItem = oPortTemp;
                }
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            PopulateCOM();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonBIN_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                Title = "Select BIN File",
                Filter = "bin files (*.bin)|*.bin"
            };

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                textBoxBIN.Text = openFile.FileName;
            }
        }

        uint Calc16CheckSum(byte[] oByteBuffer, long iLength)
        {
            // Calculate the 16 bit checksum
            uint uChecksum = 0;
            for (int i = 0; i < iLength; i++)
            {
                uChecksum += oByteBuffer[i];
            }
            uChecksum &= 0xFFFF;
            return uChecksum;
        }

        private void XferBIN()
        {
            // Get the file length
            FileInfo fi = new FileInfo(textBoxBIN.Text);

            // Load the binary data and validate the header checksum
            byte[] oBINChars = new byte[fi.Length];
            BinaryReader oData = new BinaryReader(File.Open(textBoxBIN.Text, FileMode.Open));
            oBINChars = oData.ReadBytes((int)fi.Length);
            oData.Close();

            uint uHeaderCheckSum = (fi.Length >= _nHeaderSize) ? Calc16CheckSum(oBINChars, _nHeaderSize) : 0x0;
            if (uHeaderCheckSum != _uHeaderCheckSum)
            {
                listBoxStatus.BeginInvoke(new Action(() =>
                {
                    listBoxStatus.Items.Add("Not a recognized BIN file.  Header checksum verification failed.");
                }));
                return;
            }

            Comm _pbComm = new Comm();
            _pbComm.InitializeComm(oPortTemp.PortName);
            if (_pbComm.CommEstablished())
            {
                _pbComm.WHOIS();

                // Calculate the 16 bit checksum
                uint uChecksum = Calc16CheckSum(oBINChars, fi.Length);

                listBoxStatus.BeginInvoke(new Action(() =>
                {
                    listBoxStatus.Items.Add("Sending BINLOAD " + fi.Length + " " + uChecksum);
                }));
                string sResponse = "";
                sResponse = _pbComm.BinLoad((int)fi.Length, uChecksum);
                if (!sResponse.ToLower().Contains("ok"))
                {
                    listBoxStatus.BeginInvoke(new Action(() =>
                    {
							  if (sResponse.Length == 0)
								  listBoxStatus.Items.Add("BINLOAD command did not acknowledge");
							  else
								  listBoxStatus.Items.Add("BINLOAD command did not acknowledge -> " + sResponse);
                    }));
                    _pbComm.CloseComm();
                    return;
                }

                string sHex = "";
                long nMOD = fi.Length / 20;
                for (int i = 0; i < fi.Length; i++)
                {
                    if (i % nMOD == 0)
                    {
                        listBoxStatus.BeginInvoke(new Action(() =>
                        {
                            listBoxStatus.Items.Add((i * 100.0 / fi.Length).ToString("N0") + "% completed.");
                        }));
                    }
                    if (i != 0 && (i % _nBytesPerRow == 0))
                    {
                        // Start of new row -- send previous row
                        listBoxStatus.BeginInvoke(new Action(() =>
                        {
                            if (checkBoxHEX.Checked) listBoxStatus.Items.Add("Sending BINDATA " + sHex);
                            listBoxStatus.TopIndex = listBoxStatus.Items.Count - 1;
                        }));
                        sResponse = _pbComm.BinData(sHex, false);
                        sHex = "";
                    }
                    int iByte = 0xFF & oBINChars[i];
                    sHex = sHex + iByte.ToString("X2");
                    if (i == fi.Length - 1)
                    {
                        // Send last row
                        listBoxStatus.BeginInvoke(new Action(() =>
                        {
                            if (checkBoxHEX.Checked) listBoxStatus.Items.Add("Sending BINDATA " + sHex);
                            listBoxStatus.Items.Add("BIN File transferred.");
                        }));
                        sResponse = _pbComm.BinData(sHex, true);
                    }
                }
                _pbComm.CloseComm();
                listBoxStatus.BeginInvoke(new Action(() =>
                {
                    listBoxStatus.Items.Add("BINDATA response " + sResponse);
                    listBoxStatus.Items.Add("Connection closed");
                    listBoxStatus.TopIndex = listBoxStatus.Items.Count - 1;
                }));
            }
            else
            {
                listBoxStatus.BeginInvoke(new Action(() =>
                {
                    listBoxStatus.Items.Add("Unable to connect on " + cbCOM.Text);
                }));
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            listBoxStatus.Items.Clear();

            oPortTemp = (ComPort) cbCOM.SelectedItem;
            if (oPortTemp.PortName == "")
            {
                listBoxStatus.Items.Add("COM port not selected");
                return;
            }
            if (textBoxBIN.Text == "")
            {
                listBoxStatus.Items.Add("File not selected");
                return;
            }
            if (System.IO.Path.GetExtension(textBoxBIN.Text).ToLower() != ".bin")
            {
                listBoxStatus.Items.Add("File extension is not BIN");
                return;
            }

            _oXferThread = new Thread(XferBIN);
            _oXferThread.SetApartmentState(ApartmentState.STA);
            _oXferThread.Name = "Xfer";
            _oXferThread.Start();
        }
    }

    public class PortItem
    {
        public string Port { get; set; }

        public PortItem(string sPort)
        {
            Port = sPort;
        }

        public override string ToString()
        {
            return Port;
        }
    }

    public class ComPort
    {
        public string PortName { get; set; }
        public string DiplayText { get; set; }
        public override string ToString()
        {
            return DiplayText;
        }
    }

}
